## STAGES
```
stages:
  - test
  - build

build:
  stage: build
  image: node:10
  script:
    - npm install 
    - npm run grunt
```
## STAGES PARARELL
```
build-first:
  stage: build

build-second:
  stage: build
```

## ARTIFACT IN FUTURE STAGE
```
build:
  stage: build
  script:
    - echo "Latihan Artifact" > artifact.txt
  artifacts:
    paths:
      - artifact.*

test:
  stage: test
  script:
    - cat artifact*.*
```

## ARTIFACT IN FUTURE STAGE EXPICIT FROM build-second
```
build-second:
  stage: build
  script:
    - echo "Latihan Artifact" > artifact2.txt
  artifacts:
    paths:
      - artifact2.*

view-build:
  stage: test
  dependencies:
    - build-second
  script:
    - cat artifact*.*
```

## PASING VARIABEL
- Declaring Project Variable
- Declaring Group Variable
- Using Variables In Builds

```
- ./script.sh 

'!/bin/bash
echo "I am using variable 1 from inside a script: $VARIABLE1"

build:
  script:
    - "echo Variable 1: $VARIABLE1"
    - "echo Variable 2: $VARIABLE2"
    - "echo Not a variable: $NOTAVARIABLE"
    - "echo Group variable: $GROUPVAR"
    - ./script.sh
```

## BUILDING DOCKER IMAGES
```
variables:
  DOCKER_IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA

build:
  stage: build
  image: docker:stable
  services:
    - docker:dind
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker info
  script:
    - docker build -t $DOCKER_IMAGE_TAG .
    - docker push $DOCKER_IMAGE_TAG
```

## SPEEDING UP BUILDS WITH CACHE
```
test-10:
  stage: test
  image: node:10
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
    - node_modules/
  artifacts:
    when: always
    expire_in: 1 week
    paths:
      - package-lock.json
      - npm-audit.json
```


## INTEGRATING KUBERNETES AND GITLAB-CI

- Dynamic Environtment and Review Apps
- Viewing Deployments to Environments 
- Viewing Deployment History
- Deploying an Application to Kubernetes
- Deploy Tokens & Image Pull Secret

- Integrating and functional testing
- Analyzing code quality
- Dynamic application security testing
- Application metrics collection
- Application monitoring with prometheus

- Code Quality Job
- Code Quality Artifacts 
- Code Quality Report in Merge Request

- Dynamic Security Testing
- Adding automated DAST
- Viewing DAST reports

- Promotheus
- Adding a metrics endpoint
- Viewing raw metrics

- Dedicated Runners
> Shell : Run builds directly on runner host, Useful for building virtual machine or Docker images
> Docker : Run builds in Docker containers, Can be used for building Docker images through "Docker in Docker" Configuration

> Docker Machine : Launches virtual machines to run builds in docker, Usefull for build autoscalling
> Kubernetes : Run builds in Docker containers inside kubernets, Useful to consolidate build infrastucture when using kubernetes

- Automating runner deployment
> Creating AWS instance for runners 
> Deploying runners with ansible
> Deploying a Docker Machine Runner
> Autoscalling runners using cloud resources
> Adding Gitlab Runner to Kubernets
> Running Builds on Kubernetes

Private Gitlab Server Install
> Install Gitlab Server
> Attaching a shared runner to a private Gitlab server
